<?php
	session_start();
	if (!isset($_SESSION['account'])){
		header("Location: login.php");
	}
	if (isset($_SESSION['account']))
            {
                echo 'Tên đăng nhập của bạn là : '. $_SESSION['account']['tk'];
                echo "<br>";
            }
	

	//Dap an
	$answers = [
		['question_id' => 1, 'option' => 'C'],
		['question_id' => 2, 'option' => 'A'],
		['question_id' => 3, 'option' => 'C'],
		['question_id' => 4, 'option' => 'B'],
		['question_id' => 5, 'option' => 'C'],
		['question_id' => 6, 'option' => 'C'],
		['question_id' => 7, 'option' => 'D'],
		['question_id' => 8, 'option' => 'D']
	];

	//Cac cau hoi
	$questions = [
		[
			'id' => 1,
			'question' =>'Câu 1: '.'Có một đàn vịt, cho biết 1 con đi trước thì có 2 con đi sau, 1 con đi sau thì có 2 con đi trước, 1 con đi giữa thì có 2 con đi 2 bên. Hỏi đàn vịt đó có mấy con?'
		],
		[
			'id' => 2,
			'question' => 'Câu 2: '.'Làm thế nào để qua được câu này?'
		],
		[
			'id' => 3,
			'question' => 'Câu 3: '.'Sở thú bị cháy, con gì chạy ra đầu tiên?'
		],
		[
			'id' => 4,
			'question' => 'Câu 4: '.'Bệnh gì bác sỹ bó tay?'
		],
		[
			'id' => 5,
			'question' => 'Câu 5: '.'Ở Việt Nam, rồng bay ở đâu và đáp ở đâu?'
		],
		[
			'id' => 6,
			'question' => 'Câu 6: '.'Tay cầm cục thịt nắn nắn, tay vỗ mông là đang làm gì?'
		],
		[
			'id' => 7,
			'question' => 'Câu 7: '.'Con gấu trúc ao ước gì mà không bao giờ được?'
		],
		[
			'id' => 8,
			'question' => 'Câu 8: '.'Có 1 đàn chim đậu trên cành, người thợ săn bắn cái rằm. Hỏi chết mấy con?'
		]
	];
	//Cac phuong an tra loi
	$options = [
		[
			'question_id' => 6,
			'options' => ['A' => 'Nướng thịt', 'B' => 'Thái Thịt', 'C' => 'Cho con Bú', 'D' => 'Đấu vật'] 
		],
		[
			'question_id' => 1,
			'options' => ['A' => 1, 'B' => 2, 'C' => 3, 'D' => 4] 
		],       
		[
			'question_id' => 2,
			'options' => ['A' => 'Bỏ cuộc', 'B' => 'Cho tôi qua đi mà', 'C' => 'Không thể qua', 'D' => 'Câu này khó quá'] 
		],
		[
			'question_id' => 3,
			'options' => ['A' => 'Con chim', 'B' => 'Con rắn', 'C' => 'Con người','D' => 'Con tê giác'] 
		],
		[
			'question_id' => 4,
			'options' => ['A' => 'HIV', 'B' => 'Gãy tay', 'C' => 'Siđa', 'D' => 'Bệnh sĩ'] 
		],
		[
			'question_id' => 5,
			'options' => ['A' => 'Hà Nội và Long An', 'B' => 'Hà nội và Quảng Ninh', 'C' => 'Thăng Long và Hạ long', 'D' => 'Quảng Ninh và Long An'] 
		],
		[
			'question_id' => 7,
			'options' => ['A' => 'Ăn Kẹo', 'B' => 'Uống cocacola', 'C' => 'Đá bóng', 'D' => 'Chụp hình'] 
		],
		[
			'question_id' => 8,
			'options' => ['A' => 1,'B' => 2,'C' => 14,'D' => 15] 
		]
	];


/*	 for ($i=0; $i < count($questions) ; $i++) { 
	 	echo $questions[$i]['question'];
	 	echo "<br>";
	 	for ($j=0; $j < count($options) ; $j++) { 
	 		if($questions[$i]['id'] == $options[$j]['question_id'] ){

	 			echo  "A "." ".$options[$j]['options']['A'];
	 			echo "<br>";
	 			echo  "B "." ".$options[$j]['options']['B'];
	 			echo "<br>";
	 			echo  "C "." ".$options[$j]['options']['C'];
	 			echo "<br>";
	 			echo  "D "." ".$options[$j]['options']['D'];
	 			echo "<br>";
	 			echo "<br>";
	 		}
	 	}
	 }*/

	
?>


<!DOCTYPE html>
<html>
<head>
	<title></title>
	
</head>
<body>
	<form action="" method="POST">

		<div class="container">
			<img style="width: 150px;text-align: center;" src="<?php echo $_SESSION['avatar']['link']; ?>" >
			<div class="tieude" style="font-size: 24px; margin-bottom: 10px; clear: both; text-align: center;">Trắc Nghiệm Vui</div>
			<div class="row">	
					<div style="float: right; clear: both;">
					</div>
					<!-- <img style="width: 150px; margin-left: 500px;" src="<?php echo $targetFile;?>">  -->
					<?php for ($i=0; $i < 8 ; $i++) :?>
						<?php  
							echo $questions[$i]['question']."<br>";
			 			
			 			?>
			 			<?php for ($j=0; $j < 8 ; $j++) :?>
			 					<?php if($questions[$i]['id'] == $options[$j]['question_id'] ) :?>
				 					<input type="radio" name="<?php  echo ($i+1) ?>" value="A" >
				 						<?php 
				 							$diem=0;
				 							echo  "A:"." ".$options[$j]['options']['A'];
				 							if (isset($_POST['s'])) {
				 								if (isset($_POST[$i+1])) {
				 									if ($answers[$i]['option'] == 'A') {
				 										echo "  "."ĐÚNG";	
				 									}else{echo " "."SAI";}
				 								}
				 							}
				 							echo "<br>";
				 						 ?>
					 				


					 				<input type="radio" value="B"  name="<?php echo $i+1; ?>">
					 				<?php 
				 							echo  "B:"." ".$options[$j]['options']['B'];
				 							if (isset($_POST['s'])) {
				 								if (isset($_POST[$i+1])) {
				 									if ($answers[$i]['option'] == 'B') {
				 										echo " "."ĐÚNG"; 
				 									}else{echo " "."SAI";}
				 								}
				 							}
				 							echo "<br>";
				 						 ?>
				 					
					 				
					 				<input type="radio" value="C" name="<?php echo $i+1; ?>">
					 				<?php 
				 							echo  "C:"." ".$options[$j]['options']['C'];
				 							if (isset($_POST['s'])) {
				 								if (isset($_POST[$i+1])) {
				 									if ($answers[$i]['option'] == 'C') {
				 										echo " "."ĐÚNG"; 
				 									}else{echo " "."SAI";}
				 								}
				 							}
				 							echo "<br>";
				 						 ?>
					 				
					 				
					 				<input type="radio" value="D"  name="<?php echo $i+1; ?>">
					 				<?php 
				 							echo  "D:"." ".$options[$j]['options']['D'];
				 							if (isset($_POST['s'])) {
				 								if (isset($_POST[$i+1])) {
				 									if ($answers[$i]['option'] == 'D') {
				 										echo " "."ĐÚNG"; 
				 									}else{echo " "."SAI";}
				 								}
				 							}
				 							echo "<br>";
				 							echo "<br>";
				 						 ?>	
				 					 <?php 
				 						 if(isset($_POST['s'])){
				 						 	$d =[];
				 						 	$dem = 0;
				 						 	if(isset( $_POST[$i+1])){	

					 						 	$d[] = $_POST[$i+1];
					 						 for ($o=0; $o < count($d); $o++) { 
					 						 	echo "$d[$o]".' '."ban vua chon";
					 						 	echo "<br>";
					 						
					 						 	if ($d[$o] == $answers[$i]['option']) {
					 						 		$dem++;
					 						 		echo "bạn chọn đúng"."<br>";
					 						 		echo " diem cua ban la ".$dem;
					 						 		echo "<br>";					 				
					 						 		$dem++;
					 						 		}else{
					 						 			echo "bạn đã chọn sai"."<br>";
					 						 		}
					 						 	}
					 						}else{
					 							echo "câu này chưa được chọn";
					 							echo "<br>";
				 						 		}
					 						}
				 					?>
				 						
			 				<?php endif; ?>
			 			<?php endfor; ?>
					<?php endfor; ?>
					
					<input type="submit" name="s" value="Đáp án">
					<a href="logout.php"><input type="button" name="abc" value="thoát"></a>
					 
					<?php 

						if(isset($_POST['s'])){	
							for ($m=0; $m < count($answers) ; $m++) { 
								for ($n=0; $n < count($options); $n++) { 
									if ($answers[$n]['question_id'] == $options[$m]['question_id']) {
										echo "Câu". $answers[$n]['question_id'].$answers[$m]['option']."  ";
										echo "<br>";
									}
								}
							}
						}

					 ?>
			</div>		 
		</div>
	</form>



</body>
</html>
