<form action="" method="get" accept-charset="utf-8" style="padding-bottom: 50px;width: 500px;margin: auto;">
	<input type="text" name="name" placeholder="Nhập tại đây ....">
	<input type="submit" name="submit" value="Tìm">
</form>

<table border="1" style="text-align: center;width: 1200px;margin: auto;">
	<thead>
		<tr>
			<th>STT</th>
			<th>ID</th>
			<th>Tên Giày</th>
			<th>Giá</th>
			<th>Kích Cỡ</th>
			<th>Số Lượng</th>
			<th>Thương Hiệu</th>
			<th>Loại</th>
			<th>Trạng Thái</th>
			<th colspan="2">Thao tác</th>
		</tr>
	</thead>
	<tbody>
		<?php
		if (count($products) > 0) : 
			$i = 0;
			foreach ($products as $item) :
				$i++;
		?>
		<tr>
			<td><?php echo $i;?></td>
			<td><?php echo $item['id'];?></td>
			<td><?php echo $item['name'];?></td>
			<td><?php echo $item['sku'];?></td>
			<td><?php echo $item['price'];?></td>
			<td><?php echo $item['sizes'];?></td>
			<td><?php echo $item['qty'];?></td>
			<td><?php echo $item['brand_id'];?></td>
			<td><?php echo $item['product_category_id'];?></td>
			<td><?php echo displayStatus($item['status']);?></td>
			<td>
				<a href="index.php?c=product&m=update&id=<?php echo $item['id'];?>" title="">Sửa</a>
				<a href="index.php?c=product&m=delete&id=<?php echo $item['id'];?>" title="">Xóa</a>
			</td>
		</tr>
		<?php
			endforeach;
		else: 
		?>
			<tr><td colspan="5">Chưa có bản ghi</td></tr>
		<?php
		endif; 
		?>
	</tbody>
</table>