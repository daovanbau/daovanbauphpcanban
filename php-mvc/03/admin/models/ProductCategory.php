<?php 

class ProductCategory extends Database {

	public function getCategories()
	{
		$sql = "SELECT * FROM product_categories";
		$query = $this->_connect->query($sql);
		if ($query) {
			return $query->fetch_all(MYSQLI_ASSOC);
		}

		return null;
	}

	public function getCategory($where = '')
	{
		$condition = '';
		if ($where != '') {
			$condition = 'AND ' . $where;
		}

		$sql = sprintf("SELECT * FROM product_categories WHERE 1=1 %s LIMIT 1", $condition);
		try {
			$query = $this->_connect->query($sql);
			if ($query) {
				return $query->fetch_assoc();
			}
		} catch (Exception $ex) {
			die($ex->getMessage());
		}

		return null;
	}

	public function addCategory($name,$description,$status)
	{
		$sql = "INSERT INTO product_categories (name, description, status) VALUES ('$name', '$description', '$status')";
		try {

			$query = $this->_connect->query($sql);
			if ($query) {
				return true;
			}
		} catch (Exception $ex) {
			die($ex->getMessage());
		}

		return null;
	}

	public function editCategory($id, $name, $description, $status)
	{
		$sql = "UPDATE product_categories SET name = '$name', description='$description', status='$status' WHERE id='$id' ";
		try {

			$query = $this->_connect->query($sql);
			if ($query) {
				return true;
			}
		} catch (Exception $ex) {
			die($ex->getMessage());
		}

		return false;
	}

	public function deleteCategory($id)
	{
		$sql = "DELETE FROM product_categories WHERE id=$id";
		try {

			$query = $this->_connect->query($sql);
			if ($query) {
				return true;
			}
		} catch (Exception $ex) {
			die($ex->getMessage());
		}

		return false;
	}

}