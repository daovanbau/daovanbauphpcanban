<?php

class Product extends Database {

	public function getProduct($where = '')
	{

		$condition = '';
		if ($where != '') {
			$condition = ' AND ' . $where;
		}

		$sql = sprintf("SELECT products  WHERE 1=1 %s LIMIT 1", $condition);

		try{

			$query = $this->_connect->query($sql);
			if ($query) {
			return $query->fetch_assoc();	
		}

	}catch (Exception $ex) {
		die ($ex->getMessage());
	}
		return null;
	}


	public function getProducts($where='',$limit = 10,$offset = 0,$orderby='')
    {	

    	$condition = '';
    	if ($where != '') {
    		$condition = 'AND' . $where;
    	}

		$sql = sprintf("SELECT *FROM  products WHERE 1=1 %s LIMIT %s OFFSET %s %s ",$condition,$limit,$offset,$orderby);
		try {
			$query = $this->_connect->query($sql);
			if ($query) {
				return $query->fetch_all(MYSQLI_ASSOC);
			}
		} catch (Exception $ex) {
			die ($ex->getMessage());	
		}
		
		return null;
    }


    public function editProduct($id, $name,$sku,$price,$sizes,$qty,$brand_id,$productCategory_id,$status)
	{
		$sql = "UPDATE products SET  name = '$name',sku = '$sku', prince='$prince',sizes='$sizes',qty='$qty',brand_id='$brand_id',productCategory_id='$productCategory_id',status='$status' WHERE id=$id ";
		try {

			$query = $this->_connect->query($sql);
			if ($query) {
				return true;

			}

		} catch (Exception $ex) {
			die($ex->getMessage());
		}

		return false;
	}



	public function addProduct($name, $sku,$price,$qty,$product_category_id,$status)
	{
		$sql = "INSERT INTO products(name, sku, price, qty, product_category_id, status) VALUES ('$name', '$sku',' $price', '$qty', '$product_category_id', '$status')";
		try {
			$query = $this->_connect->query($sql);
			if ($query) {
				return true;
			}
		} catch (Exception $ex) {
			die($ex->getMessage());
		}

		return null;
	}

	public function deleteProduct($id)
	{
		$sql = "DELETE FROM products WHERE id=$id";
		try {

			$query = $this->_connect->query($sql);
			if ($query) {
				return true;
			}
		} catch (Exception $ex) {
			die($ex->getMessage());
		}

		return null;
	}

}