<?php 

require 'models/Product.php';

class ProductController {

	protected $productModel;

	public function __construct()
	{
		$this->productModel = new Product();
	}

	public function index()
	{
		$data = [];
		$where = '';
		if (isset($_GET['name']) && $_GET['name'] != '') {
			$where = "name LIKE '%" . trim($_GET['name']) . "%'"; 
		}

		$products = $this->ProductModel->getProducts($where);
		$data['products'] = $products;

		return view('products.index', $data);
	}

	public function create()
	{	
		$data = $errors = [];

		if (isset($_POST['submit'])) {

			if (!isset($_POST['name']) || $_POST['name'] == '') {
				$errors[] = 'Vui lòng nhập tên sản phẩm';
			}

			if (count($errors) == 0) {
				$name = trim($_POST['name']);
				$sku = trim($_POST['sku']);
				$price = trim($_POST['price']);
				$qty = trim($_POST['qty']);
				$product_category_id = trim($_POST['product_category_id']);
				$status = trim($_POST['status']);
				$product = $this->ProductModel->addProduct($name, $sku,$price,$qty,$product_category_id,$status);
				if ($product) {
					redirect('index.php?c=product&m=index');
				}
			}
		}

		$data['errors'] = $errors;

		return view('products.create', $data);
	}

	public function update() 
	{
		$data = $errors = [];

		$id = isset($_GET['id']) ? (int) $_GET['id'] : 0;

		if ($id == 0) {
			redirect('index.php?c=product');
		}

		$where = 'id = ' . $id;
		$product = $this->productModel->getProduct($where);
		$data['product'] = $product;

		if (isset($_POST['submit'])) {
			if (!isset($_POST['name']) || $_POST['name'] == '') {
				$errors[] = 'Vui lòng nhập tên thương hiệu';
			}

			if (!isset($_POST['sku']) || $_POST['sku'] == '') {
				$errors[] = 'Vui lòng nhập sku ';
			}

			if (!isset($_POST['price']) || $_POST['price'] == '') {
				$errors[] = 'Vui lòng nhập giá ';
			}

			if (!isset($_POST['sizes']) || $_POST['sizes'] == '') {
				$errors[] = 'Vui lòng nhập kích cỡ';
			}

			if (!isset($_POST['qty']) || $_POST['qty'] == '') {
				$errors[] = 'Vui lòng nhập số lượng';
			}

			if (!isset($_POST['brand_id']) || $_POST['brand_id'] == '') {
				$errors[] = 'Vui lòng nhập ID thương hiệu';
			}

			if (!isset($_POST['product_category_id']) || $_POST['product_category_id'] == '') {
				$errors[] = 'Vui lòng nhập ID loại sản phẩm';
			}

			if (!isset($_POST['status']) || $_POST['status'] == '') {
				$errors[] = 'Vui lòng nhập trạng thái';
			}

			if (count($errors) == 0) {
				$name = trim($_POST['name']);
				$sku = trim($_POST['sku']);
				$price = trim($_POST['price']);
				$sizes = trim($_POST['sizes']);	
				$qty = trim($_POST['qty']);
				$brand_id = trim($_POST['brand_id']);
				$product_category_id = trim($_POST['product_category_id']);
				$slug = trim($_POST['slug']);
				$status = trim($_POST['status']);
				$products = $this->productModel->editProduct($id,$name,$sku,$price,$sizes,$qty,$brand_id,$product_category_id, $status);
				if ($product) {
					redirect('index.php?c=product&m=index');
				}
			}
		}

		$data['errors'] = $errors;
		return view('products.update', $data);
	}

	public function delete()
	{
		$id = isset($_GET['id']) ? (int) $_GET['id'] : 0;

		if ($id == 0) {
			redirect('index.php?c=product');
		}

		$where = 'id = ' . $id;
		$product = $this->productModel->getProduct($where);
		if (!is_null($product)) {
			$this->productModel->deleteProduct($id);
		}

		redirect('index.php?c=product');
	}

}