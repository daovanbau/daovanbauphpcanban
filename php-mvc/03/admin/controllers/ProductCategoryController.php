<?php 

require 'models/ProductCategory.php';

class ProductCategoryController {

	protected $productCategoryModel;

	public function __construct()
	{
		$this->productCategoryModel = new ProductCategory();
	}

	public function index()
	{
		$data = [];

		$categories = $this->productCategoryModel->getCategories();
		$data['categories'] = $categories;

		return view('productcategories.index', $data);
	}

	public function create()
	{
		$data = $errors = [];

		if (isset($_POST['submit'])) {
			if (!isset($_POST['name']) || $_POST['name'] == '') {
				$errors[] = 'Vui lòng nhập tên thương hiệu';
			}
			
			if (count($errors) == 0) {
				$name = trim($_POST['name']);
				$description = trim($_POST['description']);
				$status = trim($_POST['status']);
				$category = $this->productCategoryModel->addCategory($name, $description, $status);
				if ($category) {
					redirect('index.php?c=productCategory&m=index');
				}
			}
		}

		$data['errors'] = $errors;


		return view('productcategories.create',$data);
	}

	public function update()
	{
		

		$id = isset($_GET['id']) ? (int) $_GET['id'] : 0;

		if ($id == 0) {
			redirect('index.php?c=productCategory');
		}
		$where = 'id = ' . $id;
		$productCategory = $this->productCategoryModel->getCategory($where);
		$data['productCategory'] = $productCategory;

		if (isset($_POST['submit'])) {
			
			if (!isset($_POST['name']) || $_POST['name'] == '') {
				$errors[] = 'Vui lòng nhập tên thương hiệu';
			}

			if (count($errors) == 0) {
				$name = trim($_POST['name']);
				$description = trim($_POST['description']);
				$status = trim($_POST['status']);
				$productCategory = $this->productCategoryModel->editCategory($id, $name, $description, $status);
				if ($productCategory) {
					redirect('index.php?c=productCategory&m=index');
				}
			}
		}

		$data['errors'] = $errors;
		return view('productcategories.update', $data);
	}

	public function delete()
	{
		$id = isset($_GET['id']) ? (int) $_GET['id'] : 0;

		if ($id == 0) {
			redirect('index.php?c=productCategory');
		}

		$where = 'id = ' . $id;
		$productCategory = $this->productCategoryModel->getCategory($where);
		if (!is_null($productCategory)) {
			$this->productCategoryModel->deleteCategory($id);
		}

		redirect('index.php?c=productCategory');
	}
}