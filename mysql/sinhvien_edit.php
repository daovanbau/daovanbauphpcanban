
<?php 
$connect = mysqli_connect("localhost", "root","",'quanlysv') or die("Chưa liên kết được SQL");
mysqli_query($connect, 'SET NAMES UTF8');
 ?>

 <?php
 //bước 1: lấy thông tin sinh viên theo sinh viên được truyền lên thanh địa chỉ
 $masv = $_GET['masv'];
 $sql = "SELECT * FROM sinhvien WHERE masv ='{$masv}' LIMIT 1";
 $query = $connect->query($sql);
 $sinhvien = $query->fetch_assoc();
if(is_null($sinhvien)) {
	header('locahost: sinhvien.php');
} 
$errors = [];

if (isset($_POST['them'])) {
	if (!isset($_POST['masv']) || $_POST['masv'] == '') {
		$errors[] = 'Vui lòng nhập Mã SV';
	}

	if (!isset($_POST['hoten']) || $_POST['hoten'] == '') {
		$errors[] = 'Vui lòng nhập Tên SV';
	}

	if (count($errors) == 0) {
		$masv1 = trim($_POST['masv']);
		$hoten = trim($_POST['hoten']);
		$ngaysinh = trim($_POST['ngaysinh']);
		$gioitinh = trim($_POST['gioitinh']);
		$diachi = trim($_POST['diachi']);
		$email = trim($_POST['email']);
		$makhoa = trim($_POST['makhoa']);

		//Kiem tra masv co bi trung hay khong
		$sql3 = "SELECT * FROM sinhvien WHERE masv = '{$masv1}' AND masv <> '{$masv}'  LIMIT 1";
		$query = $connect->query($sql3);
		$result = $query->fetch_assoc();
		if (!is_null($result)) {
			$errors[] = 'Mã SV bị trùng.';
		} else {
			$sql4 = "UPDATE sinhvien SET hoten='$hoten',ngaysinh='$ngaysinh',gioitinh='$gioitinh' ,diachi='$diachi',email='$email',makhoa='$makhoa' WHERE masv='{$masv}'";
			$query = $connect->query($sql4);
			if ($query) {
				header('location: sinhvien.php');
			} else {
				$errors[] = 'Không cập nhật được sinh viên';
			}
		}
	}
}
?>
<section>
	<div class="container">
		<div class="message"></div>
			<form action="" method="POST">
 			<div>Mã sinh viên: <input readonly="readonly" type="text" name="masv" value="<?php if(isset($_POST['masv']))  echo $_POST['masv']; else echo $sinhvien['masv'];?>"></div>

 			<div>Tên sinh viên: <input type="text" name="hoten" value="<?php if(isset($_POST['hoten']))  echo $_POST['hoten']; else echo $sinhvien['hoten']; ?>"></div>

 			<div>Ngày sinh: <input type="text" name="ngaysinh" placeholder="yyyy-mm-dd" value="<?php if(isset($_POST['ngaysinh']))  echo $_POST['ngaysinh']; else echo $sinhvien['ngaysinh']; ?>"></div>

 			<div>Giới tính: <input type="radio" name="gioitinh" value="1" <?php if((isset($_POST['gioitinh']) && $_POST['gioitinh'] == 1) || $sinhvien['gioitinh'] == 1) echo 'checked="checked"';?>> Nam <input type="radio" name="gioitinh" value="0" <?php if((isset($_POST['gioitinh']) && $_POST['gioitinh']==0) || $sinhvien['gioitinh'] == 0) echo 'checked="checked"';?>> Nữ</div>

 			<div>Địa chỉ: <input type="text" name="diachi" value="<?php if(isset($_POST['diachi']))  echo $_POST['diachi']; else echo $sinhvien['diachi'];  ?>"></div>

 			<div>Email: <input type="text" name="email" placeholder="admin@gmail.com" value="<?php if(isset($_POST['email'])) echo $_POST['email']; else echo $sinhvien['email']; ?>"></div>

 			<div>Mã khoa: 
 			<select name="makhoa">
						<option value="">-----Chọn----</option>
						<?php
						$sql1 = "SELECT * FROM khoa";
						$query = $connect->query($sql1);
						$khoa = $query->fetch_all(MYSQLI_ASSOC);
						if(!is_null($khoa) && count($khoa) > 0):

							foreach ($khoa as $item):?>
						 <option value="<?php echo $item['makhoa'];?>" <?php if((isset($_POST['makhoa']) && $_POST['makhoa'] == $item['makhoa']) || ($sinhvien['makhoa'] == $item['makhoa'])) echo 'selected = "selected"';?>
        					<?php echo $item['makhoa'];?>><?php echo $item['makhoa']?>-<?php echo $item['ten_khoa']?></option>
						 <?php 
							endforeach;
							endif;
						 ?>
					</select>
 		</div>

 			<div><input type="submit" name="them" value="Cập nhật"></div>
 		</form>
	</div>
</section>