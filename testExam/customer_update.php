<?php 
	include'connect.php';
	$id = $_GET['id'];
	$sql = "SELECT * from customers where id ='$id' limit 1 ";
	$query = $connect->query($sql);
	$row = $query->fetch_assoc();
	$sql1 = "SELECT * from provinces";
	$query1 = $connect->query($sql1);
	$row1 = $query1-> fetch_all(MYSQLI_ASSOC);

	$sql2 = "SELECT * from districts";
	$query2 = $connect->query($sql2);
	$row2 = $query2-> fetch_all(MYSQLI_ASSOC);
	
	$sql3 = "SELECT * from customers";
	$query3 = $connect->query($sql3);
	$row3 = $query3-> fetch_all(MYSQLI_ASSOC);
	
	$sql4 = "SELECT * from customer_groups";
	$query4 = $connect->query($sql4);
	$row4 = $query4-> fetch_all(MYSQLI_ASSOC);
	$error= [];
	if(isset($_POST['submit'])){
		if(!isset($_POST['email'])|| $_POST['email']==''){
			$error[]= "không được để trống email";
		}
		if(!isset($_POST['name'])|| $_POST['name']==''){
			$error[]= "không được để trống tên";
		}
		if(!isset($_POST['phone'])|| $_POST['phone']==''){
			$error[]= "không được để trống phone";
		}
		if(!isset($_POST['r1'])|| $_POST['r1']==''){
			$error[]= "không được để trống tỉnh/thành phố";
		}
		if(!isset($_POST['r2'])|| $_POST['r2']==''){
			$error[]= "không được để trống huyện/thị xã";
		}
		if(!isset($_POST['r4'])|| $_POST['r4']==''){
			$error[]= "không được để trống nhóm người dùng";
		}
		if(count($error)==0){
			$phone = $_POST['phone'];
			$email = $_POST['email'];
			$name = $_POST['name'];	
			$address = $_POST['address'];
			$province= $_POST['r1'];
			$district= $_POST['r2'];
			$gender= $_POST['gender'];
			$customer_group_id= $_POST['r4'];
			$status = $_POST['status'];

			$sql = "SELECT * from customers where phone = '$phone' limit 1 ";
			$query = $connect->query($sql);
			$result = $query->fetch_assoc();
			var_dump($result);
			if(is_null($result)){
				header("location: customer.php");
			}else{
				$sql = "UPDATE customers set full_name='$name',email = '$email',phone='$phone',address='$address',province_id='$province',district_id='$district',gender='$gender',customer_group_id='$customer_group_id',status='$status' where id ='$id' ";
				$query = $connect->query($sql);
				if($query){
					echo " thành công";
				}
			}
		}
	}
?>

<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<form method="post">
				<a href="customer.php">back</a>
		 <p>	
	 			<?php 
	 				if(count($error) >0):
	 					for ($i=0; $i < count($error) ; $i++) :
 			?>
 					<?php echo $error[$i]; ?>
 			<?php 	
		 			endfor;
		 		endif;
 			 ?>
 		</p> 
		<table>
			<tr>
				<th>Họ tên</th>
				<td><input type="text" name="name" value="<?php echo $row['full_name'];?> "></td>
			</tr>
			<tr>
				<th>Email</th>
				<td><input type="text" name="email" value="<?php echo $row['email'] ?>"></td>
			</tr>
			<tr>
				<th>phone</th>
				<td><input type="text" name="phone" value="<?php echo $row['phone'] ?>"></td>
			</tr>
			<tr>
				<th>Địa Chỉ </th>
				<td><input type="text" name="address" value="<?php echo $row['address'] ?>"></td>
			</tr>
			<tr>
				<th>Tỉnh/Thành Phố</th>
				<td>
					<select name="r1">
						<option>--chọn--</option>
						<?php  
							if(count($row1)>0):
								foreach ($row1 as $r1):
						?>
						<option value="<?php echo $r1['id'] ?>" 
							<?php if(isset($_POST['r1'])&& $_POST['r1']==$r1['id'] || $r1['id']==$row['province_id'] )
								echo "selected='selected'";
							 ?>
						>
							<?php echo $r1['id'].$r1['name'] ?>
						</option>
						<?php  
								endforeach;
							endif;
						?>
					</select>
				</td>
			</tr>
			<tr>
				<th>Huyện/Xã</th>
				<td>
					<select name="r2">
						<option>--chọn--</option>
						<?php  
							if(count($row2)>0):
								foreach ($row2 as $r2):
						?>
						<option value="<?php echo $r2['id'] ?>" 
							<?php if(isset($_POST['r2'])&& $_POST['r2']==$r2['id'] || $r2['id']==$row['district_id'] )
								echo "selected='selected'";
							 ?>
						>
							<?php echo $r2['id'].$r2['name'] ?>
						</option>
						<?php  
								endforeach;
							endif;
						?>
					</select>
				</td>
			</tr>
			<tr>
				<th>Giới Tính</th>
				<td>
					Nam<input type="radio" name="gender" value="1"<?php if(isset($_POST['gender'])&&$_POST['gender']==1 || $row['gender'] == 1)echo "checked='checked'" ?>>
					Nữ<input type="radio" name="gender" value="0"<?php if(isset($_POST['gender'])&&$_POST['gender']==0 || $row==0)echo "checked='checked'" ?>>
				</td>
			</tr>
			<tr>
				<th>Nhóm người dùng</th>
				<td>
					<select name="r4">
						<option>--chọn--</option>
						<?php  
							if(count($row4)>0):
								foreach ($row4 as $r4):
						?>
						<option value="<?php echo $r4['id'] ?>" 
							<?php if(isset($_POST['r4'])&& $_POST['r4']==$r4['id'] || $r4['id']==$row['customer_group_id'] )
								echo "selected='selected'";
							 ?>
						>
							<?php echo $r4['id'].$r4['name'] ?>
						</option>
						<?php  
								endforeach;
							endif;
						?>
					</select>
				</td>
			</tr>

			<tr>
				<th>Trạng Thái</th>
				<td><input type="text" name="status" value="<?php echo $row['status'] ?>"></td>
			</tr>
			<tr>
				<th>Tác Vụ</th>
				<td><input type="submit" name="submit" value="Sửa" ></td>
			</tr>
		</table>
	</form>
</body>
</html>